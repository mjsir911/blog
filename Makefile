MDFILES=$(wildcard *.md)
FILES=$(MDFILES:.md=.html)
%.html: %.md
	pandoc --standalone --filter=auto-header.jq --from markdown --to html --variable date="$$(git log --format=%aD $< | tail -n 1)" < $< > $@


public/:
	mkdir $@

public/%: % | public/
	cp --parents $< $|

.PHONY: public
public: $(addprefix public/,$(FILES))

.DEFAULT_GOAL := public.tar.gz
public.tar.gz: public
	tar --create $< | gzip > $@

clean:
	$(RM) public.tar.gz
	$(RM) -r public
