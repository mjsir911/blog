# Athanasia

What does athanasia mean?
=========================

This seems like such an obscure word, barely mentioned on internet searches &
definitions seem to just sum it up to be an identical synonym to immortality.

Side not: vim's spellchecker does not even register athanasia as a real word.

So, I'm going to narrow down the definition to what I believe the word means. I
think I got the feel of the narrowed down definition from one of CGP-Grey's
podcasts on immortality, where he mentioned that he wants to be immortal *and
young*, which he described as athanasia (I think).


So that's what I believe it means. athanasia is a subset of immortality where
you stay functionally young.
