#!/bin/sh
# this next line is ignored by jq, which otherwise does not continue comments \
exec jq -ef "$0"

.meta.title = {
	"t": "MetaString", 
	"c": (
		[([.blocks[] | select(.t == "Header") | .c] | try sort_by(.[0]))[0][2][]] |
		map(if .t == "Space" then " " else .c end) | join("")
	)
} 
| del(.blocks[0])

# .blocks[0].t = ([.blocks[] | select(.t == "Header") | .c] | sort_by(.[0]))[0][1][0]
