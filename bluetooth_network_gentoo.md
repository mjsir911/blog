# Getting bluetooth networking on gentoo was a pain to figure out

## Initial debugging
When setting up a bluetooth device with `bt-network --connect "phone name here"
nap`, and then typing in `nmcli`, I was getting a `plugin missing`
message under the bluetooth interface in red text.

```
$ nmcli
bnep0: unmanaged
        "Intel 0a2b"
        bluetooth (unknown), 01:23:45:67:89:AB, plugin missing, sw, mtu 1500
```

## Executable debug mode

I then started investigating the debug logs of networkmanager

```
$ NetworkManager --debug --log-level=DEBUG --plugins=bluetooth
...
<warn>  [1551316482.0957] settings: could not load plugin 'bluetooth' from file '/usr/lib64/NetworkManager/1.14.4/libnm-settings-plugin-bluetooth.so': No such file or directory
...
```

On further investigation of the source code, `--plugins=bluetooth` is not the way to do this and
the correct file path is
`/usr/lib64/NetworkManager/1.14.4/libnm-device-plugin-bluetooth.so`

It turns out, even though I was compiling networkmanager with USE flags
`bluetooth` (on gentoo), the file
`/usr/lib64/NetworkManager/1.14.4/libnm-device-plugin-bluetooth.so` was not
getting created because the build process requires `modemmanager`

```
NetworkManager $ grep -r libnm-settings-plugin-bluetooth.so
Makefile.in:@WITH_MODEM_MANAGER_1_TRUE@	$(srcdir)/tools/check-exports.sh $(builddir)/src/devices/bluetooth/.libs/libnm-device-plugin-bluetooth.so "$(srcdir)/linker-script-devices.ver"
Makefile.in:@WITH_MODEM_MANAGER_1_TRUE@	$(call check_so_symbols,$(builddir)/src/devices/bluetooth/.libs/libnm-device-plugin-bluetooth.so)
```

## Fix

So the fix was merely adding the USE flag `modemmanager`, `USE=modemmanager
emerge net-misc/networkmanager` fixes this issue, but this is not clear at all.


After doing a bit more research, found a bug report detailing this problem
exactly [here](https://bugs.gentoo.org/661054). What I would do for better SEO
on the bug reporter.


# Additional symptoms

I started dealing with these problems after attempting to press the "Network
Access Point" button on the blueman-manager application, resulting in a python
traceback

```
blueman-manager 20.25.52 WARNING  ManagerDeviceMenu:157 err       : disconnect failed g-io-error-quark: GDBus.Error:org.freedesktop.DBus.Python.TypeError: Traceback (most recent call last):
  File "/usr/lib64/python3.6/site-packages/dbus/service.py", line 707, in _message_cb
    retval = candidate_method(self, *args, **keywords)
  File "/usr/lib64/python3.6/site-packages/blueman/main/DbusService.py", line 32, in wrapper
    return method(*args[1:], **kwargs)
  File "/usr/lib64/python3.6/site-packages/blueman/plugins/applet/DBusService.py", line 99, in disconnect_service
    if not self.parent.Plugins.run_ex("service_disconnect_handler", cb, service, ok, err):
  File "/usr/lib64/python3.6/site-packages/blueman/main/PluginManager.py", line 217, in run_ex
    ret = getattr(inst, func)(*args, **kwargs)
  File "/usr/lib64/python3.6/site-packages/blueman/plugins/applet/NMPANSupport.py", line 33, in service_disconnect_handler
    conn.deactivate()
  File "/usr/lib64/python3.6/site-packages/blueman/main/NetworkManager.py", line 113, in deactivate
    self.client.deactivate_connection_async(self.active_connection, None, on_connection_deactivate)
TypeError: Argument 1 does not allow None as a value
 (36)
```

Wierd stuff
