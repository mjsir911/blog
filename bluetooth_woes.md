So, I changed the distro on my desktop from ubuntu -> manjaro

a common setup for me is to watch videos on my laptop, stream the audio to my
desktop and play video games while listening to the video audio


This worked *mostly* fine in ubuntu, when connecting from my laptop I could
always get my laptop as an a2dp_sink with:

```
pacmd set-card-profile bluez... a2dp_sink
```

but moving over to manjaro caused a whole slew of problems.

Of note is that a lot of these problems were intermittent, when booting from
windows or a live ubuntu usb stick things would work a bit better, but after
the first reboot they want back to the way they were before.

1) bluetooth card would not work unless rebotting from ubuntu or windows
: solution: [add `btusb` to `/etc/modules...` to load it at boot](
	https://forum.manjaro.org/t/bluetooth-only-works-after-being-first-enabled-in-windows/75201/2)

2) when connecting to desktop from laptop, bluetoothd would straight up *crash*
: solution: [disable bluetooth LE on desktop by adding `ControllerMode = bredr` to `/etc/bluetooth/main.conf`.](
	https://flx.ai/2019/06/13/bose-qc35ii-i3#deactivate-bluetooth-le)

3) This is incidentally fixed by the previous two, but only the machine *doing*
the connecting can claim `a2dp_sink`. If I connect from my laptop, my laptop
can stream to my desktop. If I connect from my desktop, my desktop can stream
to my laptop. Not exactly sure of the fix to this yet.
