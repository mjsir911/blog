# HTML in mailing lists

A while back, I was investigating mailing lists on [sr.ht](lists.sr.ht). I was
a bit peeved when I was testing and found that they rejected all html emails. I
asked on their irc channel, and was met with explanations boiling down to "html
bad", which I don't disagree with, but don't believe email should be purged of
all `text/html` emails.

## Accessibility is important

When you tie mailing lists to filing issues, you need to make those mailing
lists as accessible as possible. Sure, *I* could configure my email client to
send out plaintext-only emails (although its a real pain), but I can't expect
everyone to know how to configure their email client for this.

And on a more personal note, filing an issue or pull request for your project
should not require me to reconfigure software on **my** machine.

## Widespread belief

I was recently looking to send to other, unrelated, mailing lists. I found this
same rule for many other mailing lists.

> Note also that the list only accepts plain-text email; please disable HTML in your outgoing messages. 

> Spam filters are also more likely to reject HTML-formatted messages; please use plain text.

This is not singled-out to just sr.ht, but a widespread belief! Does the
widespread developer community believe end-users with no knowledge of the
complications of email mime-types should not be able to contribute to
discussion?


## My personal problems

The reason I am actually upset about this is because I send html-formatted
emails all the time, to clients and friends, with very small amounts of
formatting (mostly links and code blocks). I've jury-rigged my mail client
(neomutt) to let me write emails in markdown and on send it translates them to
a multipart markdown/html email. I am very content with this, and it just
works. 

Except when a mailing list rejects anything with html.



## Solution?

Choosing a default is hard. On the one hand, I need to send formatted emails to
clients (`text/html`). On the other, I would like to send emails to mailing
lists (no `text/html`).

I could have a variable defaulted to empty string containing all mime types I
want to translate to.

I think simple solution is to just bind `Y`(`y` is what I use for fancy
multipart) to just send email as-is.
