# Cracking my keepassxc password

## Attempt 1: john the ripper and hashcat

I tried following the tutorial [here](https://www.rubydevices.com.au/blog/how-to-hack-keepass)
 but:

1) john the ripper does not work on keepassxc's kdbx files
2) hashcat does not work on my laptop with no gpu

## attempt 1.5: custom python script

I then tried writing my own python script but got caught up on making the
generic leet transformation function and gave up on the whole project for a
while

## attempt 2: keepass-simple-crack-kit + HashcatRulesEngine

My new attempt was using
[keepass-simple-crack-kit](https://github.com/abcarroll/keepass-simple-crack-kit)
and [HashcatRulesEngine](https://github.com/llamasoft/HashcatRulesEngine).

problems:
1) keepass-simple-crack-kit was broken because the opening code failed, looks
like this can't handle keepassxc's kdbx files either
2) I didn't see a way to only replace individual characters with
hashcatrulesengine. for example I wanted to change only a single `l` in
`hello`, but I couldn't come up with a rule, kept getting `he11o`

## attempt 3: custom scripts part 2

Finally went back to my own custom scripts with the previous expreiences, I
decided on a more hollistic approach of rule engines as files piping into
eachother. I developed two rules, `replace-combinations` which just replaces
any instace of one character with another (does not deal with multiple
characters nor replacing multiple at the same time) `rules/replace-combinations`:

```python
#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

from sys import argv, stdin

import re


def replacenth(string, sub, wanted, n):
    pattern = re.compile(sub)
    where = [m for m in pattern.finditer(string)][n-1]
    before = string[:where.start()]
    after = string[where.end():]
    newString = before + wanted + after

    return newString


from_char = argv[1]
to_char = argv[2]

for line in stdin:
    line = line.strip()
    print(line)
    for i in range(line.count(from_char)):
        print(replacenth(line, from_char, to_char, i))
```


And also a leet file just for helpin `rules/leet`:

```
#!/usr/bin/env bash

./rules/replace-combinations e 3 | ./rules/replace-combinations i 1 | ./rules/replace-combinations o 0
```


to generate the word list:
```
cat password.file  | ./rules/replace-combinations i e | ./rules/replace-combinations E e | ./rules/replace-combinations E '#' | ./rules/leet.sh  | sort -u | ./rules/replace-combinations n N | ./rules/replace-combinations r R > word.list
```

and finally, to replace `keepass-simple-crack-kit`:
```
$ for line in $(cat word.list); do keepassxc-cli open password.kdbx <<< "$line" |& grep Error || echo "$line" ; done | grep -v Error
```

Very homebrew, but it got the job done after a bit.

My word count was at 1000 words and it took a few minutes to find the right
one.
