```
09:09 < mjsir911> Hi, I'm having trouble updating lineageOS (logs here: https://pastebin.com/tz3G5ApZ). Could anyone help me out?
09:09 < BtbN> Just do it manually?
09:10 < mjsir911> Does the update menu in the settings not work?
09:10 < BtbN> Well, it clearly doesn't look like it, does it?
```

On my LineageOS device, when I go to Settings -> About Phone -> LineageOS
Updates -> and press install on any version, it comes back to me with: "Install
Error"

When plugging it in to a USB port with adb debugging turned on, running `adb
logcat` gives the error message: `failed to set bootloader message: failed to
find /misc partition`

# TLDR:

As root, remount root (`/`) partition and then 
add this line:
```
/dev/block/bootdevice/by-name/misc      /misc              emmc    defaults                                        defaults
```
to the file `/fstab${ro.hardware}` (just press tab after /fstab), and then
automatic updates will run. This change is not persistent and you have to run
it on each reboot.

I found another pastebin from a year ago here:
[https://pastebin.com/vURbvSGr](https://pastebin.com/vURbvSGr)

My first intuition was to investigate the service called `uncrypt`, since that
is what was giving me the error message

```bash
sumire:/ $ find / 2> /dev/null | grep uncrypt
/system/etc/init/uncrypt.rc
```

A single file, an init file (I think?) which contents point to a binary in
`/system/bin/uncrypt` which doesnt actually exist.

```bash
sumire:/ $ ls /system/bin 2>&1 |  grep uncr
ls: /system/bin/uncrypt: Permission denied
```

Turns out I just don't have permissions for this file, so onto root!

```bash
adb root
```


While investingating how to create `/dev/socket/uncrypt` to sucessfully run the
uncrypt command by hand, a user in the #lineageOS irc channel responded to a
question of mine:

```
09:15 < mjsir911> Anyone know what the /misc partition is and why it doesnt exist on my phone?
09:36 < s17> The misc partition is used by recovery and is 4KB or larger.
09:37 < s17> https://source.android.com/devices/bootloader/partitions-images
09:38 < mjsir911> ty s17, do you know if it is installed by default?
```

This gives me a clue, although I'm still not sure why the misc partition still
doesn't exist on my machine. 


Success! As root I found a device symlinked at `/dev/block/bootdevice/by-name/misc`

```bash
sumire:/ # readlink -f /dev/block/bootdevice/by-name/misc
/dev/block/mmcblk0p33
```

After searching around for fstabs for a while...

```irc
09:44 < mjsir911> in my bootloader I found a symlink in `/dev/block/bootdevice/by-name/misc` so I think uncrypt just wasnt finding it in `/etc/fstab`
09:48 < mjsir911> welp, `/etc/fstab` isnt a thing on android, whoops
09:50 < BtbN> Android very much has an fstab
09:52 < mjsir911> Where's it located? It's not in the place I was expecting
09:54 <+Khaytsus> Most of android's mounting is done in init scripts, but there is a fstab in /system
10:06 < mjsir911> alright so dumping a line for my /misc partition it /fstab.qcom fixed the updater, but on reboot that line when away. Anyway for persistent fstabs?
```

And looking at the `uncrypt` source code
(`./bootable/recovery/uncrypt/uncrypt.cpp`):
```cpp
static struct fstab* read_fstab() {
	fstab = NULL;

	// The fstab path is always "/fstab.${ro.hardware}".
	char fstab_path[PATH_MAX+1] = "/fstab.";
	if (!property_get("ro.hardware", fstab_path+strlen(fstab_path), "")) {
		ALOGE("failed to get ro.hardware");
		return NULL;
	}
	...
}
```

Turns out the fstab for my phone was located at `/fstab.qcom`:
```bash
sumire:/ # ls -lha /fstab.qcom
-rw-r----- 1 root root 2.0K 1969-12-31 19:00 /fstab.qcom
```

Notice that only root can read & write from this file.

adding a line to that file (`/fstab.qcom`) for my `/misc` partition did indeed
fix the automatic updater, but that line went away after any reboots

```fstab
/dev/block/bootdevice/by-name/misc       /misc     vfat defaults,noauto                                                               defaults
```


I experimented with extracting the boot image and changing fstab, but `/misc`
was already in it!

```fstab
/dev/block/bootdevice/by-name/misc      /misc              emmc    defaults                                        defaults
```

At this point, I don't know much about the architecture and so I can't really
find why that line isn't showing up on a booted system

If anyone has any pointers, feel free to send me an email.
