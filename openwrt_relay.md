Just a note if setting up relayd on an openwrt router to relay another router.

My wan configuration looks like this:
```
config interface 'wwan'
	option proto 'dhcp'
	option ipaddr '192.168.1.2'
```

Notice how I have set a default requested IP address so when my relay is
configured like this:
```
config interface 'repeater'
	option proto 'relay'
	option ipaddr '192.168.1.2'
```

this requires no configuration of the upstream router to get it working.

This bit me when the upstream router was reflashed but forgot to assign a
static address to this router.


This is similar to the [`dhcp-requested-address` option in `dhclient`](https://superuser.com/a/487608/785195)

Thanks a lot to Kufat on the #openwrt channel on freenode for helping me track
down this undocumented option.
