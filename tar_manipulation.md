# How to edit tar files without extracting to the filesystem?


Sometimes I have a problem like "I want to move a subdirectory up a level in a
tar archive and I don't want to do a bunch of stuff with files on the
filesystem"

[how to do a tar extract | create pipe on the fly?](https://www.linuxquestions.org/questions/linux-general-1/how-to-do-a-tar-extract-%7C-create-pipe-on-the-fly-609651/)


This is the answer to that.


```
tar -f "${DISTDIR}/rust-weechat-39219d770f1684a3ff05bf7853c87bd019592f9e.crate" \
	-x \
	--wildcards 'rust-weechat-39219d770f1684a3ff05bf7853c87bd019592f9e/weechat-*' \
	--strip-components=1 \
	--to-command="tar -h -u -f ${DISTDIR}"'/"${TAR_FILENAME%%/*}.crate" /dev/stdin --transform="s@.*@$TAR_FILENAME@"' \
```
